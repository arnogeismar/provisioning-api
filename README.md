# Prerequisites

1) Java 8 jdk installed [https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

2) docker installed [https://docs.docker.com/install/](https://docs.docker.com/install/)

3) docker-compose installed [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

4) Maven installed [https://maven.apache.org/install.html](https://maven.apache.org/install.html)


# Important

If you have postgres already installed and are on a linux distribution you should disable the postgres service by running 
`/etc/init.d/postgresql stop`
We are going to run postgres in a container using docker-compose and otherwise the port may conflict in userland

## Running the project

1) Open a terminal and go to the root of the project
`docker-compose up -d`

2) Open the project in IntelliJ or Eclips and run class ProvisioningApiApplication.

## What is it?

The project is a POC of an asynchronous rest api with a eventsourced backend.

## Functionality

1) create clients with basic validation (email valid, name, description, companyName not null/empty.
2) list clients
3) project events on a postgresql database
4) replay events on the projection (postgresql)

### Create clients

`curl -X POST \
  http://localhost:8080/api/clients \
  -H 'content-type: application/json' \
  -d '{
	"name": "hello hello",
	"companyName": "some company name",
	"description": "description",
	"email": "arnogeismargmail.com"
}'`

basic validation applies:
1) email must be valid
2) all other fields cannot be null or empty

### List clients

`curl -X GET http://localhost:8080/api/clients`

### Replay
1) Truncate all database tables in postgres
2) `curl -X GET http://localhost:8080/api/projections/reset `
3) All data is recreated in postgresql

This proves that the events are the source of truth, not the postgresql data.
The events can be replayed on any datastore based on the specific requirements for functionality (ie. a cache, mongodb, couchdb, cassandra, ...)

### Websocket feedback

1) in `resources/static` open index.html and click `connect`
2) This opens up a view on the commands which are being applied and the events which are in turn created. 


