package com.kpn.iot.thingsengine.provisioningapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class ClientCreatedResponseDTO {
    private String message;
    private UUID commandId;
}
