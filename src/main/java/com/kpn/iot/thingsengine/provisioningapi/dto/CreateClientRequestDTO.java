package com.kpn.iot.thingsengine.provisioningapi.dto;

import lombok.Data;

@Data
public class CreateClientRequestDTO {
    private String name;

    private String companyName;

    private String description;

    private String email;
}
