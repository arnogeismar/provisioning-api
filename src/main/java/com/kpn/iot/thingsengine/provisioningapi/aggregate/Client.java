package com.kpn.iot.thingsengine.provisioningapi.aggregate;

import com.kpn.iot.thingsengine.provisioningapi.event.ClientCreatedEvt;
import com.kpn.iot.thingsengine.provisioningapi.command.CreateClientCmd;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.util.StringUtils;

import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.regex.Pattern;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
@Slf4j
public class Client {

    private static final Pattern EMAIL_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    @AggregateIdentifier
    private UUID id;

    private String name;

    private String companyName;

    private String description;

    private String email;

    private ZonedDateTime createDate;

    private ZonedDateTime updateDate;

    private String createUser;

    private String updateUser;

    @CommandHandler
    public Client(CreateClientCmd cmd) {
        log.debug("Handling cmd {}", cmd);
        if(StringUtils.isEmpty(cmd.getName())){
            throw new IllegalArgumentException(cmd.getId()+"/CONSTRAINT Name must be filled in");
        }
        if(StringUtils.isEmpty(cmd.getDescription())) {
            throw new IllegalArgumentException(cmd.getId()+"/CONSTRAINT Description must be filled in");
        }
        if(StringUtils.isEmpty(cmd.getEmail()) || !EMAIL_REGEX.matcher(cmd.getEmail()).find()) {
            throw new IllegalArgumentException(cmd.getId()+"/CONSTRAINT Email must be a valid email format");
        }
        if(StringUtils.isEmpty(cmd.getCompanyName())){
            throw new IllegalArgumentException(cmd.getId()+"/CONSTRAINT Company name must be filled in");
        }
        apply(new ClientCreatedEvt(cmd.getId(), cmd.getName(), cmd.getCompanyName(), cmd.getDescription(), cmd.getEmail(), cmd.getCreateDate(), cmd.getUpdateDate(),cmd.getCreateUser(), cmd.getUpdateUser()));
    }

    @EventSourcingHandler
    public void handle(ClientCreatedEvt evt) {
        log.debug("Applying {}", evt);
        this.id = evt.getId();
        this.name = evt.getName();
        this.companyName = evt.getCompanyName();
        this.description = evt.getDescription();
        this.email = evt.getEmail();
        this.createDate = evt.getCreateDate();
        this.updateDate = evt.getUpdateDate();
        this.createUser = evt.getCreateUser();
        this.updateUser = evt.getUpdateUser();
    }
}
