package com.kpn.iot.thingsengine.provisioningapi.event;

import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.axonframework.serialization.Revision;

import java.time.ZonedDateTime;
import java.util.UUID;

@Value
public class ClientCreatedEvt {

    @TargetAggregateIdentifier
    private UUID id;

    private String name;

    private String companyName;

    private String description;

    private String email;

    private ZonedDateTime createDate;

    private ZonedDateTime updateDate;

    private String createUser;

    private String updateUser;
}
