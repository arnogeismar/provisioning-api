package com.kpn.iot.thingsengine.provisioningapi.query;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Data
public class ClientView extends BaseEntity {

    @Id
    private UUID id;

    @NotNull
    @NotBlank
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "description")
    private String description;

    @Email
    @Column(name = "email", nullable = false)
    private String email;

    public ClientView() {
    }

    public ClientView(@NotNull ZonedDateTime createDate, ZonedDateTime updateDate, @NotNull String createUser, String updateUser, UUID id, @NotNull @NotBlank String name, String companyName, String description, @Email String email) {
        super(createDate, updateDate, createUser, updateUser);
        this.id = id;
        this.name = name;
        this.companyName = companyName;
        this.description = description;
        this.email = email;
    }
}
