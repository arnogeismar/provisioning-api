package com.kpn.iot.thingsengine.provisioningapi.rest.socket;

public class StatusMessage {

    private String uuid;
    private String message;

    public StatusMessage() {
    }

    public StatusMessage(String uuid, String message) {
        this.uuid = uuid;
        this.message = message;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
