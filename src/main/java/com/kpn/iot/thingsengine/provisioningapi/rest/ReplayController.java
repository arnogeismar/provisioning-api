package com.kpn.iot.thingsengine.provisioningapi.rest;

import org.axonframework.config.EventProcessingModule;
import org.axonframework.eventhandling.TrackingEventProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/projections/reset")
public class ReplayController {

    @Autowired
    private EventProcessingModule eventProcessingModule;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getClients(){
        eventProcessingModule.eventProcessorByProcessingGroup("projections",
                TrackingEventProcessor.class)
                .ifPresent(trackingEventProcessor -> {
                    trackingEventProcessor.shutDown();
                    trackingEventProcessor.resetTokens();
                    trackingEventProcessor.start();
                });
        return ResponseEntity.ok().build();
    }
}
