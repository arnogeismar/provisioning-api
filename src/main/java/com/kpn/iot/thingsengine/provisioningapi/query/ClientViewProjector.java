package com.kpn.iot.thingsengine.provisioningapi.query;

import com.kpn.iot.thingsengine.provisioningapi.dto.ClientListViewResponse;
import com.kpn.iot.thingsengine.provisioningapi.event.ClientCreatedEvt;
import com.kpn.iot.thingsengine.provisioningapi.query.repository.ClientViewRepository;
import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.ResetHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@ProcessingGroup("projections")
public class ClientViewProjector {

    private ClientViewRepository clientViewRepository;

    public ClientViewProjector(ClientViewRepository clientViewRepository) {
        this.clientViewRepository = clientViewRepository;
    }

    @EventHandler
    public void handle(ClientCreatedEvt evt) {
        clientViewRepository.save(new ClientView(evt.getCreateDate(), evt.getUpdateDate(),
                evt.getCreateUser(), evt.getUpdateUser(), evt.getId(), evt.getName(), evt.getCompanyName(), evt.getDescription(), evt.getEmail()));

    }

    @QueryHandler
    public ClientListViewResponse handle(ClientViewQuery clientViewQuery) {
        ClientListViewResponse clientViews = new ClientListViewResponse();
        clientViews.addAll(clientViewRepository.findAll());
        return clientViews;
    }

    @ResetHandler
    public void reset(){
        clientViewRepository.deleteAll();
    }

}
