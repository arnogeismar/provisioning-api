package com.kpn.iot.thingsengine.provisioningapi.dto;

import com.kpn.iot.thingsengine.provisioningapi.query.ClientView;

import java.util.ArrayList;

public class ClientListViewResponse extends ArrayList<ClientView> {
}
