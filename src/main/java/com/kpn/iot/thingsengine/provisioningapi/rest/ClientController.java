package com.kpn.iot.thingsengine.provisioningapi.rest;

import com.kpn.iot.thingsengine.provisioningapi.command.CreateClientCmd;
import com.kpn.iot.thingsengine.provisioningapi.dto.ClientCreatedResponseDTO;
import com.kpn.iot.thingsengine.provisioningapi.dto.ClientListViewResponse;
import com.kpn.iot.thingsengine.provisioningapi.dto.CreateClientRequestDTO;
import com.kpn.iot.thingsengine.provisioningapi.query.ClientViewQuery;
import com.kpn.iot.thingsengine.provisioningapi.rest.socket.FutureDelegator;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api/clients")
public class ClientController {

    private CommandGateway commandGateway;

    private QueryGateway queryGateway;

    private FutureDelegator futureDelegator;

    public ClientController(CommandGateway commandGateway, QueryGateway queryGateway, FutureDelegator futureDelegator) {
        this.commandGateway = commandGateway;
        this.queryGateway = queryGateway;
        this.futureDelegator = futureDelegator;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ClientCreatedResponseDTO> createClient(@RequestBody CreateClientRequestDTO createClientRequestDTO) throws Exception {
        UUID commandUUID = UUID.randomUUID();
        CompletableFuture<Object> cpf = commandGateway.send(new CreateClientCmd(commandUUID, createClientRequestDTO.getName(), createClientRequestDTO.getCompanyName(), createClientRequestDTO.getDescription(), createClientRequestDTO.getEmail(), ZonedDateTime.now(), ZonedDateTime.now(), "SYSTEM", "SYSTEM"));
        futureDelegator.delegate(cpf);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ClientCreatedResponseDTO("Intent registered for command", commandUUID));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientListViewResponse> getClients(){
        ClientListViewResponse clientViews = queryGateway.query(new ClientViewQuery(), ClientListViewResponse.class).join();
        return ResponseEntity.of(Optional.of(clientViews));
    }


}
