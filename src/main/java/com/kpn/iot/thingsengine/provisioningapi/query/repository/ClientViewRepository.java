package com.kpn.iot.thingsengine.provisioningapi.query.repository;

import com.kpn.iot.thingsengine.provisioningapi.query.ClientView;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ClientViewRepository extends JpaRepository<ClientView, UUID> {
    List<ClientView> findAllByNameContainsOrDescriptionContainsOrEmailContainsAllIgnoreCase(Pageable pageable, String name, String description, String email);
}

