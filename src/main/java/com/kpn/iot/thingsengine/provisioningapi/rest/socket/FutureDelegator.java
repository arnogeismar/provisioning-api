package com.kpn.iot.thingsengine.provisioningapi.rest.socket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Component
public class FutureDelegator {

    @Autowired
    private Websocket websocket;

    public void delegate(CompletableFuture<Object> completableFuture){
        completableFuture.whenCompleteAsync((result, throwable) -> {
            try {
                if(result != null) {
                    websocket.clientCreatedTopic((UUID)result);
                } else {
                    String[] messages = throwable.getMessage().split("/");
                    websocket.errorTopic(UUID.fromString(messages[0]),messages[1]);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
