package com.kpn.iot.thingsengine.provisioningapi.command;

import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.time.ZonedDateTime;
import java.util.UUID;

@Value
public class CreateClientCmd {

    @TargetAggregateIdentifier
    private UUID id;

    private String name;

    private String companyName;

    private String description;

    private String email;

    private ZonedDateTime createDate;

    private ZonedDateTime updateDate;

    private String createUser;

    private String updateUser;

}
