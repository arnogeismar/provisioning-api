package com.kpn.iot.thingsengine.provisioningapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvisioningApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProvisioningApiApplication.class, args);
    }

}
