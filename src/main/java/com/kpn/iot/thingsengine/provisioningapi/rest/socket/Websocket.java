package com.kpn.iot.thingsengine.provisioningapi.rest.socket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class Websocket {

    private final SimpMessageSendingOperations messagingTemplate;

    public Websocket(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void clientCreatedTopic(UUID uuid) throws Exception {
        log.debug("sending message");
        messagingTemplate.convertAndSend("/topic/tracker",new StatusMessage(uuid.toString(), "Successfully completed command"));
    }

    public void errorTopic(UUID uuid, String errormessage) throws Exception {
        log.debug("sending error message");
        messagingTemplate.convertAndSend("/topic/tracker", new StatusMessage(uuid.toString(), errormessage));
    }
}
