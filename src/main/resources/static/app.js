var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('http://localhost:8080/websocket/tracker');
    stompClient = StompJs.Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/tracker', function (message) {
            showConfirm(JSON.parse(message.body));
        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}


function showConfirm(object) {
    if(object.message.startsWith("CONSTRAINT")) {
        $("#confirms").append("<tr style='background-color:red;'><td style='color:white;'>" + object.message +" "+ object.uuid+ "</td></tr>");
    } else {
        $("#confirms").append("<tr style='background-color:green;'><td style='color:white;'>" + object.message +" "+ object.uuid+ "</td></tr>");
    }

}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
});